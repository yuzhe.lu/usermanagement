package model

import (
	"fmt"

	"github.com/gomodule/redigo/redis"
	"github.com/jinzhu/gorm"
	//"ihomebj5q/conf"
)

//var GlobalDB *gorm.DB
var GlobalRedis redis.Pool
var db *gorm.DB
// User gorm user object
type User struct {
	ID       int32       `gorm:"type:int(11);primary key"`
	Username string      `gorm:"type:varchar(64);unique;not null"`
	Nickname string      `gorm:"type:varchar(128)"`
	Passwd   string      `gorm:"type:varchar(32);not null"`
	Skey     string      `gorm:"type:varchar(16);not null"`
	Headurl  string      `gorm:"type:varchar(128);unique;not null"`
	Uptime   int64      `gorm:"type:datetime"`
}

func InitDb()error{
	//打开数据库
	//拼接链接字符串
	//connString := conf.MysqlName+":"+conf.MysqlPwd+"@tcp("+conf.MysqlAddr+":"+conf.MysqlPort+")/"+conf.MysqlDB+"?parseTime=true"

	//db,err  := gorm.Open("mysql",connString)
	db,err  := gorm.Open("mysql", "root:rootroot@tcp(127.0.0.1:3306)/test?charset=utf8")
	if err != nil {
		fmt.Println("链接数据库失败",err)
		return err
	}

	//连接池设置
	//设置初始化数据库链接个数
	db.DB().SetMaxIdleConns(50)
	db.DB().SetMaxOpenConns(70)
	//db.DB().SetConnMaxLifetime(60 * 5)
	//db.LogMode(true)

	//默认情况下表名是复数
	//db.SingularTable(true)

	//GlobalDB = db

	//表的创建
	//return db.AutoMigrate(new(User),new(House),new(Area),new(Facility),new(HouseImage),new(OrderHouse)).Error
	return nil
}

func closeDB() {
	db.Close()
}
//初始化redis链接
func InitRedis(){
	GlobalRedis = redis.Pool{
		MaxIdle:20,
		MaxActive:50,
		IdleTimeout:60 * 5,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp","127.0.0.1:6379")
		},
	}
}