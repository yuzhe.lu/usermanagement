package main

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"

)

type Student struct {

	Id      int
	Name string
	Age  int
}

// 创建全局连接池句柄
var GlobalConn *gorm.DB

func main(){
	//连接数据库
	conn, err := gorm.Open("mysql", "root:rootroot@tcp(127.0.0.1:3306)/ginessential?charset=utf8")
	//gorm.Open(mysql.Open("root:rootroot@tcp(127.0.0.1:3306)/ginessential?charset=utf8"), &gorm.Config{})
	if err != nil {
		panic("fail to connect database, err: " + err.Error())
		return
	}
	defer conn.Close()



	GlobalConn = conn
	// 初始数
	GlobalConn.DB().SetMaxIdleConns(10)
	// 最大数
	GlobalConn.DB().SetMaxOpenConns(100)

	// 对比redis连接池：不需要使用 Get() 方法，取一条连接。
	// 不要复数表名
	GlobalConn.SingularTable(true)

	// 借助 gorm 创建数据库表 students
	//fmt.Println(GlobalConn.AutoMigrate(new(Student)).Error)

	//InsertData()
	//SearchData()
	UpdateData2()
}

// insert into student(name, age) values('zhangsan', 100)


//增
func InsertData()  {
	// 先创建数据 --- 创建对象
	var stu Student
	stu.Name = "lisi"
	stu.Age = 90

	// 插入(创建)数据
	fmt.Println("插入数据")
	fmt.Println(GlobalConn.Create(&stu).Error)

}
//查询
func SearchData()  {
	var stu Student

	//获取 user 表中的第一条数据
	//相当于SQL：SELECT * FROM student ORDER BY id LIMIT 1;
	GlobalConn.First(&stu)
	fmt.Println("第一条数据:",stu)


	//只查询 name、age 不查询其他值：
	GlobalConn.Select("name, age").First(&stu)
	fmt.Println("name age",stu)

	//获取 user 表中的所有数据。

	var stu2 []Student       // 改为切片
	GlobalConn.Select("name, age").Find(&stu2)
	fmt.Println("切片",stu2)   // Find() 查询多条
	//相当于SQL：select name, age from student;


	//select name, age from student where name = ‘lisi’;
	GlobalConn.Select("name, age").Where("name = ?", "lisi").Find(&stu)
	fmt.Println("lisi",stu)
	//GlobalConn.Where("name = ?", "lisi").Select("name, age").Find(&stu)


	//select name, age from student where name = ‘lisi’ and age = 22
	GlobalConn.Select("name, age").Where("name = ?", "lisi").
		Where("age = ?", 22).Find(&stu)
	//GlobalConn.Select("name, age").Where("name = ? and age = ?", "lisi", 22).Find(&stu)
}


//更新
func UpdateData() {
	//没有就插入
	var stu Student
	stu.Name = "wangwu"
	stu.Age = 99 // 没有指定 id -- 没有主键!
	fmt.Println(GlobalConn.Save(&stu).Error)
}
func UpdateData2() {
	var stu Student
	stu.Name = "wangwu"
	stu.Age = 77
	stu.Id = 3 //指定 id -- 更新操作!
	fmt.Println(GlobalConn.Save(&stu).Error)


	//Model(new(Student): 指定更新 “student” 表
	//Where("name = ?", "zhaoliu")： 指定过滤条件。
	//Update("name", "lisi").Error)：指定 把 “zhaoliu” 更新成 “lisi”
	fmt.Println(GlobalConn.Model(new(Student)).Where("name = ?", "zhaoliu").
		Update("name", "lisi").Error)
}

func deletct(){
//软删除--- 使用 Delete() 参数，指定要删除的数据所在表的表名。
	fmt.Println(GlobalConn.Where("name = ?", "lisi").Delete(new(Student)).Error)

//查询
	var stu []Student
	GlobalConn.Find(&stu)
	fmt.Println(stu)
	//想 查询 “软删除”的数据
	GlobalConn.Unscoped().Find(&stu)

}

