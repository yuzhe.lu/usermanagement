package utils

import (
    "testing"
)

func Test_Md5String(t *testing.T) {
    s := "username129"
    result := Md5String(s)
    if result != "c67f6e6860c4361205aa581f4af6071c" {
        t.Error("test failed: ", result)
    }
}

func Test_GenerateToken(t *testing.T) {
    s := "username129"
    result := GenerateToken(s)
    if result != "f7718230f1c7145ac96406a5ca53ffd5" {
        t.Error("test failed: ", result)
    }
}
