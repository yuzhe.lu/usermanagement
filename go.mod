module usermanagement

go 1.16

require (
	github.com/astaxie/beego v1.12.4-0.20201213113623-093f97636558
	github.com/gin-gonic/gin v1.7.2
	github.com/go-redis/redis v6.14.2+incompatible
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	google.golang.org/grpc v1.26.0
	google.golang.org/protobuf v1.23.0
	gopkg.in/yaml.v2 v2.4.0
)
