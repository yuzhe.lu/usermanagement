package rpcclient

import (
	"context"
	"net/http"
	"strconv"

	"github.com/astaxie/beego/core/logs"

	codeModule "usermanagement/code"
	pb "usermanagement/proto"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)



// clientWrap 方便服务的连接和关闭
type clientWrap struct {
	conn *grpc.ClientConn
	client pb.UserServiceClient
}

// getClientWrap 连接grpc服务，打包返回grpc连接和UserService的客户端
func getClientWrap () (*clientWrap, error){

	conn, err := grpc.Dial("127.0.0.1:9999",grpc.WithInsecure())
	if err != nil {

		return nil, err
	}
	// 初始化grpc的客户端
	client := pb.NewUserServiceClient(conn)

	return &clientWrap{conn: conn, client: client}, nil
}

// freeClientWrap
func freeClientWrap (wrap *clientWrap) {
	err := wrap.conn.Close()
	if err != nil {
		logs.Error("cannot close the grpc connect",err.Error())
	}
}

// Login 登录操作  userlogin handler
func Login (args map[string]string) (int, string, map[string]interface{}) {
	// 获得uuid
	uuid := args["uuid"]
	// 与grpc建立连接
	client, err := getClientWrap()
	if err != nil {
		logs.Error(uuid, " <userclient><Login> cannot connect to the grpc server, error: ", err.Error())
		return http.StatusInternalServerError, "", gin.H{"code": codeModule.CodeInternalErr , "msg": codeModule.CodeMsg[codeModule.CodeInternalErr], "data": nil}
	}
	// 确保资源关闭
	defer freeClientWrap(client)

	// 将uuid存入context上下文作为本次操作的标识        AppendToOutgoingContext returns a new context with the provided kv merged
	ctx := metadata.AppendToOutgoingContext(context.Background(), "uuid", uuid)

	//调用远程服务
	rsp, err := client.client.Login(ctx, &pb.LoginRequest{Username: args["username"], Passwd: args["passwd"]})
	if err != nil {
		logs.Error(uuid, " -- <userclient><Login> Failed to communicate with TCP server, err:", err.Error())
		return http.StatusOK, "", gin.H{"code": codeModule.CodeErrBackend, "msg": codeModule.CodeMsg[codeModule.CodeErrBackend], "data": nil}
	}

	logs.Debug(uuid, " <userclient><Login> Get token:", rsp.Token)
	var token string
	logs.Debug(uuid, " -- <userclient><Login> Succ get token:", rsp.Token, " code:", rsp.Code)
	if rsp.Code == codeModule.CodeSucc && rsp.Token != "" {
		token = rsp.Token
	}
	return http.StatusOK, token, gin.H{"code": rsp.Code,"msg": rsp.Msg, "username": rsp.Username, "nickname": rsp.Nickname, "headurl":rsp.Headurl,"token":token}
}


// Logout : user logout
func Logout(args map[string]string) (int, map[string]interface{}) {
	// get uuid
	uuid := args["uuid"]
	// communicate with rcp server
	client, err := getClientWrap()
	if err != nil {
		logs.Error(uuid, " -- <userclient><Logout> Failed to getRPCClient, err:", err.Error())
		return http.StatusInternalServerError, gin.H{"code": codeModule.CodeInternalErr, "msg": codeModule.CodeMsg[codeModule.CodeInternalErr], "data": nil}

	}
	// 确保资源关闭
	defer freeClientWrap(client)

	ctx := metadata.AppendToOutgoingContext(context.Background(), "uuid", uuid)
	//调用远程服务
	rsp, err := client.client.Logout(ctx, &pb.CommRequest{Token: args["token"], Username: args["username"]})
	if err != nil {
		logs.Error(uuid, " -- <userclient><Logout> Failed to communicate with TCP server, err:", err.Error())
		return http.StatusOK, gin.H{"code": codeModule.CodeErrBackend, "msg": codeModule.CodeMsg[codeModule.CodeErrBackend], "data": nil}
	}
	logs.Debug(uuid, "（<userclient><Logout> Succ to get response from backend with ", rsp.Code, " and msg:", rsp.Msg)
	return http.StatusOK, gin.H{"code": rsp.Code, "msg": rsp.Msg, "data": nil}

}

// GetUserinfo  get user info
func GetUserinfo(args map[string]string)(int ,map[string]interface{}){
	//get uuid
	uuid:= args["uuid"]
	//communicate with rcp server
	client, err := getClientWrap()
	if err != nil {
		logs.Error(uuid, " -- <userclient><GetUserinfo> Failed to getRPCClient, err:", err.Error())
		return http.StatusInternalServerError, gin.H{"code": codeModule.CodeInternalErr, "msg": codeModule.CodeMsg[codeModule.CodeInternalErr], "data": nil}
	}

	// 确保资源关闭
	defer freeClientWrap(client)
	ctx := metadata.AppendToOutgoingContext(context.Background(), "uuid", uuid)
	//调用远程服务
	rsp, err := client.client.GetUserInfo(ctx, &pb.CommRequest{Token: args["token"], Username: args["username"]})
	if err != nil {
		logs.Error(uuid, " -- <userclient><GetUserinfo> Failed to communicate with TCP server, err:", err.Error())
		return http.StatusOK, gin.H{"code": codeModule.CodeErrBackend, "msg": codeModule.CodeMsg[codeModule.CodeErrBackend], "data": nil}
	}
	response:= gin.H{"code": rsp.Code, "msg": rsp.Msg, "data": map[string]string{"username":rsp.Username, "nickname":rsp.Nickname, "headurl":rsp.Headurl}}
	return http.StatusOK, response
}

// EditUserinfo edit user nickname/headurl
func EditUserinfo(args map[string]string)(int ,map[string]interface{}){
	//get uuid
	uuid:= args["uuid"]

	headurl := args["headurl"]
	// get connection
	client, err := getClientWrap()
	if err != nil {
		logs.Error(uuid, " -- <userclient><EditUserinfo> Failed to getRPCClient, err:", err.Error())
		return http.StatusInternalServerError, gin.H{"code": codeModule.CodeInternalErr, "msg": codeModule.CodeMsg[codeModule.CodeInternalErr], "data": nil}
	}
	defer freeClientWrap(client)

	//updata userinfo
	mode, _ := strconv.Atoi(args["mode"])
	ctx := metadata.AppendToOutgoingContext(context.Background(), "uuid", uuid)
	editRsp, err := client.client.EditUserInfo(ctx,
		&pb.EditRequest{Username: args["username"], Token: args["token"], Nickname: args["nickname"], Headurl: headurl, Mode: uint32(mode)})
	if err != nil {
		logs.Error(uuid, " -- <userclient><EditUserinfo> Failed to communicate with TCP server, err:", err.Error())
		return http.StatusOK, gin.H{"code": codeModule.CodeErrBackend, "msg": codeModule.CodeMsg[codeModule.CodeErrBackend], "data": nil}
	}
	data := map[string]string{}
	if editRsp.Code == 0 && headurl != "" {
		data["headurl"] = headurl
	}
	return http.StatusOK,gin.H{"code": int(editRsp.Code), "msg":editRsp.Msg, "data": data}

}

// Auth user getUserInfo to auth
func Auth(args map[string]string) (int, int, string) {
	// get uuid
	uuid := args["uuid"]
	// communicate with rcp server
	client, err := getClientWrap()
	if err != nil {
		logs.Error(uuid, " -- <userclient><EditUserinfo> Failed to getRPCClient, err:", err.Error())
		return http.StatusInternalServerError, codeModule.CodeInternalErr, codeModule.CodeMsg[codeModule.CodeInternalErr]
	}
	defer freeClientWrap(client)

	ctx := metadata.AppendToOutgoingContext(context.Background(), "uuid", uuid)
	rsp, err := client.client.GetUserInfo(ctx, &pb.CommRequest{Token: args["token"], Username: args["username"]})
	if err != nil {
		logs.Error(uuid, " -- <userclient><EditUserinfo> Failed to communicate with TCP server, err:", err.Error())
		return http.StatusOK, codeModule.CodeErrBackend, codeModule.CodeMsg[codeModule.CodeErrBackend]
	}
	if rsp.Code == 0 {
		return http.StatusOK, codeModule.CodeSucc, codeModule.CodeMsg[codeModule.CodeSucc]
	}
	return http.StatusOK, int(rsp.Code), rsp.Msg
}

