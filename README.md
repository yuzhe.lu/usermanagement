# 用户管理系统
## 需求
实现用户管理系统，具体功能：  

1. 用户登陆（后台校验用户登陆信息，判断能否进入相关界面）
2. 用户可以编辑自己的nickname和图片
3. 确保测试数据库中包含10,000,000条用户账号信息.

## 设计要求
1. 分别实现HTTP server和TCP server，
2. 主要的功能逻辑放在TCP server实现
3. 用户账号信息必须存储在MySQL数据库。通过MySQL Go client连接数据库
4. 使用基于Auth/Session Token的鉴权机制，Token存储在redis，避免使用JWT等加密的形式。
5. TCP server需要提供RPC API，RPC机制希望自己设计实现
6. Web server不允许直连MySQL、Redis。所有HTTP请求只处理API和用户输入，具体的功能逻辑和数据库操作，需要通过RPC请求TCP server完成
7. 尽可能使用Go标准库
8. 安全性
9. 鲁棒性

## 思维导图
![1](pic/xmd.png)
## 数据库设计

	type User struct {
    
    ID       int32       `gorm:"type:int(11);primary key"`
    Username string      `gorm:"type:varchar(64);unique;not null"`
    Nickname string      `gorm:"type:varchar(128)"`
    Passwd   string      `gorm:"type:varchar(32);not null"`
    Skey     string      `gorm:"type:varchar(16);not null"`
    Headurl  string      `gorm:"type:varchar(128);unique;not null"`
    Uptime   int64      `gorm:"type:datetime"`
	}

数据过多，采用分表结构

![3](pic/d2.png)
![2](pic/d1.png)
## 页面
![5](pic/界面2.png)
![4](pic/界面1.png)
## api设计

|  类型   | 名称  |
|  ----  | ----  |
| POST  | v1/login |
| POST  | /v1/logout |
| GET | /v1/getuserinfo |
| POST  | /v1/login |
|POST |/v1/uploadpic|
## 功能1:用户登陆/v1/login
![6](pic/login.png)
## 功能2:获得用户信息 /v1/getuserinfo
![7](pic/getuserinfo.png)
## 功能3：修改用户名/v1/editnickname
![8](pic/edit-nickname.png)
![9](pic/db-nickname.png)
## 功能4:修改用户头像/v1/uploadpic
![10](pic/editpic.png)
## 功能5:登出/v1/logout
![11](pic/logout.png)


## 压力测试
### 性能
1. 必须确保返回结果是正确的
2. 每个请求都要包含RPC调用以及Mysql或Redis访问
3. 200并发（固定用户）情况下，HTTP API QPS大于3000
4. 200个client（200条TCP连接），每个client模拟一个用户（因此需要200个不同的固定用户账号）
5. 200并发（随机用户）情况下，HTTP API QPS大于1000
6. 200个client（200条TCP连接），每个client每次随机从10,000,000条记录中选取一个用户，发起请求
7. 如果涉及到鉴权，可以使用一个测试用的token，指向不同的用户
8. 2000并发（固定用户）情况下，HTTP API QPS大于1500
9. 2000并发（随机用户）情况下，HTTP API QPS大于800

增加tcp连接数    
 	```sudo sysctl -w kern.ipc.somaxconn=2048```

增加打开文件最大数.  
	```sudo sysctl -w kern.maxfiles=12288```

修改最大连接数.  
		```ulimit -n 10000```
## 200并发。固定用户
tps=2322.61。  
响应时间（90%）=66ms    
qps=并发数/平均响应时间=200/66ms=3030
![11](pic/性能测试/200固定.png)
## 200并发，随机用户
tps=11163.17    
响应时间（90%）=13ms    
qps=并发数/平均响应时间=200/13ms=15384    
![11](pic/性能测试/200随机.png)
## 1000并发，固定用户

tps=3424.18    
响应时间（90%）=271ms    
qps=并发数/平均响应时间=1000/271ms=3690![13](pic/性能测试/1000固定.png)
## 2000并发，随机用户
tps=6439.40。  
响应时间（90%）=226ms    
qps=并发数/平均响应时间=2000/226ms=8849    
![12](pic/性能测试/2000随机.png)