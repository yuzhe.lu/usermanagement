package main

import (
	"net/http"
	"path"
	"strings"

	"github.com/astaxie/beego/core/logs"
	"github.com/gin-gonic/gin"

	codeModule "usermanagement/code"
	"usermanagement/rpcclient"
	"usermanagement/utils"
)




// homePage 主页
func homePage(context *gin.Context) {
	context.String(http.StatusOK, "EntryTask - User Information System")
}



// loginHandler 登录方法
func loginHandler (c *gin.Context) {
	// 获取post表单中传入的数据
	username := c.PostForm("username")
	passwd := c.PostForm("passwd")

	logs.Debug("passwd:", passwd, "username:", username)

	//生成token
	uuid := utils.GenerateToken(username)
	logs.Debug(uuid, " -- <loginHandle> loginHandler access from:", username, "@", passwd)

	// RPC communicate
	ret, token, rsp := rpcclient.Login(map[string]string{"username":username, "passwd":passwd, "uuid":uuid})
	// set cookie 设置cookie
	if ret == http.StatusOK && token != "" {
		c.SetCookie("token", token, config.Logic.Tokenexpire, "/", config.Server.IP, false, true)
		logs.Debug(uuid, " -- <loginHandle> Set token ", token, "with expire:", config.Logic.Tokenexpire)
	}
	// 将结果返回前端
	logs.Debug(uuid, " -- <loginHandle> Succ get response from backend with", rsp["code"], " and msg:", rsp["msg"])
	c.JSON(ret, rsp)
}

// logout 登出方法
func logoutHandler(c* gin.Context) {
	// check params
	username := c.PostForm("username")
	token, err := c.Cookie("token")
	if err != nil {
		logs.Error("<handler:logoutHandler> Failed to get token from cookie, err:", err.Error())
		c.JSON(http.StatusBadRequest,  gin.H{"code": codeModule.CodeTokenNotFound, "msg": codeModule.CodeMsg[codeModule.CodeTokenNotFound], "data": nil})
		return
	}

	if len(token) != 32 {
		logs.Error("<handler:logoutHandler> Invalid token :", token)
		c.JSON(http.StatusBadRequest, gin.H{"code": codeModule.CodeInvalidToken, "msg": codeModule.CodeMsg[codeModule.CodeInvalidToken], "data": nil})
		return
	}
	uuid := utils.GenerateToken(username)
	logs.Debug(uuid, " -- <handler:logoutHandler> logoutHandler access from:", username, " with token:", token)

	// RPC communicate
	ret, rsp := rpcclient.Logout(map[string]string{"username":username, "token":token, "uuid":uuid})

	logs.Debug(uuid, " -- <handler:logoutHandler> Succ to get response from backend with ", rsp["code"], " and msg:", rsp["msg"])
	c.JSON(ret, rsp)
}

// get user info
func getUserinfoHandler(c* gin.Context) {
	// check params
	username := c.Query("username")
	token, err := c.Cookie("token")
	logs.Debug("access from:", username, " with token:", token)
	if err != nil {
		logs.Error("<handler:getUserinfoHandler> Failed to get token from cookie, err:", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"code": codeModule.CodeTokenNotFound, "msg": codeModule.CodeMsg[codeModule.CodeTokenNotFound], "data": nil})
		return
	}

	if len(token) != 32 {
		logs.Error("Invalid token :", token)
		c.JSON(http.StatusBadRequest, gin.H{"code": codeModule.CodeInvalidToken, "msg": codeModule.CodeMsg[codeModule.CodeInvalidToken], "data": nil})
		return
	}

	uuid := utils.GenerateToken(username)
	logs.Debug(uuid, " -- <handler:getUserinfoHandler> getUserinfoHandler access from:", username, " with token:", token)
	// communicate
	ret, rsp := rpcclient.GetUserinfo(map[string]string{"username":username, "token":token, "uuid":uuid})
	logs.Debug(uuid, " -- <handler:getUserinfoHandler> Succ to get response from backend with ", rsp["code"], " and msg:", rsp["msg"])
	c.JSON(ret, rsp)
}

//edit nickname
func editNicknameHandler(c* gin.Context){
	// check params
	username := c.PostForm("username")
	nickname :=c.PostForm("nickname")
	token, err := c.Cookie("token")
	logs.Debug("access from:", username, " with token:", token, " and newname:", nickname)
	if err != nil {
		logs.Error("<handler:editNicknameHandler> Failed to get token from cookie, err:", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"code": codeModule.CodeTokenNotFound, "msg": codeModule.CodeMsg[codeModule.CodeTokenNotFound], "data": nil})
		return
	}
	if len(token) != 32 {
		logs.Error("Invalid token :", token)
		c.JSON(http.StatusBadRequest,gin.H{"code": codeModule.CodeInvalidToken, "msg": codeModule.CodeMsg[codeModule.CodeInvalidToken], "data": nil})
		return
	}
	uuid := utils.GenerateToken(username)
	logs.Debug(uuid, " -- <handler:editNicknameHandler> editNicknameHandler access from:", username, " with token:", token, " new nickname:", nickname)
	// communicate
	ret, rsp := rpcclient.EditUserinfo(map[string]string{"username":username, "token":token, "nickname":nickname, "headurl":"", "mode": "1", "uuid":uuid})

	logs.Debug(uuid, " -- <handler:editNicknameHandler> Succ to get response from backend with ", rsp["code"], " and msg:", rsp["msg"])
	c.JSON(ret, rsp)
}

// generate upload image file name
func generateImgName(fname, postfix string) string {
	ext := path.Ext(fname)
	fileName := strings.TrimSuffix(fname, ext)
	fileName = utils.Md5String(fileName + postfix)

	return fileName + ext
}

// uploadHeadurlHandle
func uploadHeadurlHandler(c* gin.Context) {
	// check params
	username := c.Query("username")
	token, err := c.Cookie("token")
	logs.Debug("access from:", username, " with token:", token)
	if err != nil {
		logs.Error("<handler:uploadHeadurlHandler> Failed to get token from cookie, err:", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"code": codeModule.CodeTokenNotFound, "msg": codeModule.CodeMsg[codeModule.CodeTokenNotFound], "data": nil})
		return
	}

	uuid := utils.GenerateToken(username)
	logs.Debug(uuid, " -- <handler:uploadHeadurlHandler> uploadHeadurlHandler access from:", username, " with token:", token)
	// step 1 : auth
	httpCode, tcpCode, msg := rpcclient.Auth(map[string]string{"username":username, "token":token, "uuid":uuid})
	if httpCode != http.StatusOK || tcpCode != 0 {
		logs.Error(uuid, " -- <handler:uploadHeadurlHandler> uploadHeadurlHandler Auth failed, msg:", msg)
		c.JSON(httpCode, gin.H{"code": tcpCode, "msg": msg, "data": nil})
		return
	}
	logs.Debug(uuid, " -- <handler:uploadHeadurlHandler> uploadHeadurlHandler Auth succ")
	// step 2 : save upload picture into file
	// save picture
	file, image, err := c.Request.FormFile("picture")
	if err != nil {
		logs.Error(file,image)
		logs.Error(uuid, " -- <handler:uploadHeadurlHandler> Failed to FormFile, err:", err.Error())
		c.JSON(http.StatusOK, gin.H{"code": codeModule.CodeFormFileFailed, "msg": codeModule.CodeMsg[codeModule.CodeFormFileFailed], "data": nil})
	}

	//// check image
	if image == nil {
		logs.Error(uuid, " -- <handler:uploadHeadurlHandler> Failed to get image from formfile!")
		c.JSON(http.StatusOK, gin.H{"code": codeModule.CodeFormFileFailed, "msg": codeModule.CodeMsg[codeModule.CodeFormFileFailed], "data": nil})
		return
	}
	//// check filesize
	size, err := utils.GetFileSize(file)
	if err != nil {
		logs.Error(uuid, " -- <handler:uploadHeadurlHandler> Failed to get filesize, err:", err.Error())
		c.JSON(http.StatusOK,gin.H{"code": codeModule.CodeFileSizeErr, "msg": codeModule.CodeMsg[codeModule.CodeFileSizeErr], "data": nil})
		return
	}
	if size == 0 || size > config.Image.Maxsize * 1024 * 1024 {
		logs.Error(uuid, " -- <handler:uploadHeadurlHandler> Filesize illegal, size:", size)
		c.JSON(http.StatusOK, gin.H{"code": codeModule.CodeFileSizeErr, "msg": codeModule.CodeMsg[codeModule.CodeFileSizeErr], "data": nil})
		return
	}
	logs.Debug(uuid, " -- <handler:uploadHeadurlHandler> uploadHeadurlHandler CheckImage succ")
	//// save
	imageName := generateImgName(image.Filename, username)
	fullPath  := config.Image.Savepath + imageName

	if err = c.SaveUploadedFile(image, fullPath); err != nil {
		logs.Error(uuid, " -- <handler:uploadHeadurlHandler> Failed to save file, err:", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"code": codeModule.CodeInternalErr, "msg": codeModule.CodeMsg[codeModule.CodeInternalErr], "data": nil})
		return
	}
	logs.Debug(uuid, " -- <handler:uploadHeadurlHandler> Succ to save upload image, path:", fullPath)

	// step 3 : update picture info
	imageURL := config.Image.Prefixurl + "/" + fullPath
	//调用远程方法
	ret, editRsp := rpcclient.EditUserinfo(map[string]string{"username": username, "token": token, "nickname": "", "headurl": imageURL, "mode": "2", "uuid":uuid})
	logs.Debug(uuid, " -- <handler:uploadHeadurlHandler> editUserInfo response:", ret)
	c.JSON(ret, editRsp)
}