package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"

	"github.com/astaxie/beego/core/logs"
	"github.com/gin-gonic/gin"

	"usermanagement/conf"
)



var config conf.HTTPConf
func init(){
	// parser config
	var confFile string
	flag.StringVar(&confFile, "c", "conf/httpserver.yaml", "config file")
	flag.Parse()

	err := conf.ConfParser(confFile, &config)
	if err != nil {
		logs.Critical("Parser config failed, err:", err.Error())
		os.Exit(-1)
	}

	// init log
	logConfig := fmt.Sprintf(`{"filename":"%s","level":%s,"maxlines":0,"maxsize":0,"daily":true,"maxdays":%s}`,
		config.Log.Logfile, config.Log.Loglevel, config.Log.Maxdays)
	logs.SetLogger(logs.AdapterFile, logConfig)
	logs.EnableFuncCallDepth(true)
	logs.SetLogFuncCallDepth(3)
	logs.Async()

}

func main(){


	//启动
	//gin.SetMode(gin.ReleaseMode)
	//gin.DefaultWriter = ioutil.Discard
	//初始化路由
	engine := gin.Default()
	//
	engine.LoadHTMLFiles("./static/*")
	engine.StaticFS("/static",http.Dir("./static"))
	//路由匹配
	engine.Any("/welcome", webRoot)

	engine.POST("/v1/login", loginHandler)
	engine.POST("/v1/logout", logoutHandler)
	engine.GET("/v1/getuserinfo", getUserinfoHandler)
	engine.POST("/v1/editnickname", editNicknameHandler)
	engine.POST("/v1/uploadpic", uploadHeadurlHandler)

	//测试随机用户
	engine.POST("/v1/randomlogin", randomLoginHandler)

	//设置静态路径
	//engine.Static("/view/", "./view/")
	//engine.Static("/upload/images/", "./view/")

	// 启动运行
	engine.Run(":8080")

}

func webRoot(context *gin.Context) {
	context.String(http.StatusOK, "项目开始")
}